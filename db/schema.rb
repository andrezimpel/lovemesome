# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150308130513) do

  create_table "categories", force: :cascade do |t|
    t.string   "title",       limit: 191
    t.string   "handle",      limit: 191
    t.text     "description", limit: 65535
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.boolean  "active",      limit: 1,     default: false
    t.integer  "position",    limit: 4
  end

  add_index "categories", ["active"], name: "index_categories_on_active", using: :btree
  add_index "categories", ["handle"], name: "index_categories_on_handle", using: :btree
  add_index "categories", ["position"], name: "index_categories_on_position", using: :btree

  create_table "posts", force: :cascade do |t|
    t.string   "title",             limit: 191
    t.text     "body",              limit: 65535
    t.string   "handle",            limit: 191
    t.integer  "author_id",         limit: 4
    t.string   "type",              limit: 191
    t.string   "featured_image_id", limit: 191
    t.datetime "published_at"
    t.datetime "deleted_at"
    t.text     "excerpt",           limit: 65535
    t.string   "origin_title",      limit: 191
    t.string   "origin_url",        limit: 191
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "category_id",       limit: 4
  end

  add_index "posts", ["deleted_at"], name: "index_posts_on_deleted_at", using: :btree
  add_index "posts", ["handle"], name: "index_posts_on_handle", using: :btree
  add_index "posts", ["published_at"], name: "index_posts_on_published_at", using: :btree
  add_index "posts", ["title"], name: "index_posts_on_title", using: :btree

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id",        limit: 4
    t.integer  "taggable_id",   limit: 4
    t.string   "taggable_type", limit: 191
    t.integer  "tagger_id",     limit: 4
    t.string   "tagger_type",   limit: 191
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name",           limit: 191
    t.integer "taggings_count", limit: 4,   default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "uploads", force: :cascade do |t|
    t.string   "image_id",   limit: 191
    t.integer  "post_id",    limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "uploads", ["deleted_at"], name: "index_uploads_on_deleted_at", using: :btree
  add_index "uploads", ["image_id"], name: "index_uploads_on_image_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 191, default: "", null: false
    t.string   "encrypted_password",     limit: 191, default: "", null: false
    t.string   "reset_password_token",   limit: 191
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 191
    t.string   "last_sign_in_ip",        limit: 191
    t.string   "firstname",              limit: 191
    t.string   "lastname",               limit: 191
    t.string   "username",               limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
