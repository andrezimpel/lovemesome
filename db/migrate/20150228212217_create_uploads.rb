class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.string :image_id
      t.integer :post_id
      t.datetime :deleted_at

      t.timestamps null: false
    end
    add_index :uploads, :image_id
    add_index :uploads, :deleted_at
  end
end
