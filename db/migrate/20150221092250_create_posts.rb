class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.string :handle
      t.integer :author_id
      t.string :type
      t.string :featured_image_id
      t.datetime :published_at
      t.datetime :deleted_at
      t.text :excerpt
      t.string :origin_title
      t.string :origin_url

      t.timestamps null: false
    end
    add_index :posts, :title
    add_index :posts, :handle
    add_index :posts, :published_at
    add_index :posts, :deleted_at
  end
end
