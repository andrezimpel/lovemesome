json.extract! @upload, :id, :image_id, :post_id, :deleted_at, :created_at, :updated_at
json.image attachment_url(@upload, :image, :fill, 845, 0)
json.thumb attachment_url(@upload, :image, :fill, 100, 100)
json.filelink attachment_url(@upload, :image, :fill, 845, 0)
