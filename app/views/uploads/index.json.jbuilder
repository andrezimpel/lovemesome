json.array!(@uploads) do |upload|
  json.extract! upload, :id, :post_id, :deleted_at
  json.url upload_url(upload, format: :json)
  json.image attachment_url(upload, :image, :fill, 1000, 0)
  json.thumb attachment_url(upload, :image, :fill, 200, 150)
end
