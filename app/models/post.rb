# == Schema Information
#
# Table name: posts
#
#  id                :integer          not null, primary key
#  title             :string(191)
#  body              :text(65535)
#  handle            :string(191)
#  author_id         :integer
#  type              :string(191)
#  featured_image_id :string(191)
#  published_at      :datetime
#  deleted_at        :datetime
#  excerpt           :text(65535)
#  origin_title      :string(191)
#  origin_url        :string(191)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  category_id       :integer
#

class Post < ActiveRecord::Base

  # associations
  has_many :uploads
  belongs_to :category
  has_one :author, :foreign_key => 'id', :class_name => "User"


  # fetured image
  attachment :featured_image


  # tags
  acts_as_taggable


  # validations
  validates :title, presence: true
  # validates :featured_image, presence: true


  # filter
  default_scope { order('published_at DESC, created_at DESC') }
  scope :published, lambda { where(['published_at IS NOT NULL']) }


  # search
  searchable do
    # The :more_like_this option must be set to true
    text :title, :more_like_this => true
    text :handle, :more_like_this => true
    text :body, :more_like_this => true
    time :published_at
  end


  # navigation
  def next
    self.class.published.where("id > ?", id).first
  end
  def previous
    self.class.published.where("id < ?", id).last
  end


  # html to markdown before save
  before_save :convert_to_markdown
  def convert_to_markdown
    self.body = ReverseMarkdown.convert body
    self.excerpt = ReverseMarkdown.convert excerpt
  end


  # generate handle before create
  before_create :generate_handle
  def generate_handle
    self.handle = self.title.parameterize if self.title != ""
  end


  # author
  def author
    User.find(author_id)
  end


  # methods
  def handle_status(params)
    if params[:commit] == "Publish"
      self.update(published_at: DateTime.now)
    end

    if params[:commit] == "Unpublish"
      self.update(published_at: nil)
    end
  end


  # methods
  def published?
    published_at != nil
  end

  def unpublished?
    published_at == nil
  end

  def status
    if published?
      return "Published"
    else
      return "Draft"
    end
  end
end
