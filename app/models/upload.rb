# == Schema Information
#
# Table name: uploads
#
#  id         :integer          not null, primary key
#  image_id   :string(191)
#  post_id    :integer
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Upload < ActiveRecord::Base
  # associations
  belongs_to :post


  # fetured image
  attachment :image
end
