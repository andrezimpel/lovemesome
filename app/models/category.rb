# == Schema Information
#
# Table name: categories
#
#  id          :integer          not null, primary key
#  title       :string(191)
#  handle      :string(191)
#  description :text(65535)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  active      :boolean          default("0")
#  position    :integer
#

class Category < ActiveRecord::Base
  # associations
  has_many :posts


  # filter
  default_scope { order('position ASC') }
  scope :active, lambda { where(['active IS TRUE']) }


  # validations
  validates :title, presence: true


  # path
  def to_param
    "#{handle}"
  end


  # generate handle before create
  before_create :generate_handle
  def generate_handle
    self.handle = self.title.parameterize if self.title != ""
  end
end
