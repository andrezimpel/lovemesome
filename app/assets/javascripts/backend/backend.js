//= require jquery
//= require jquery_ujs

//= require redactor
//= require imagemanager
//= require refile
//= require bootstrap-tokenfield/dist/bootstrap-tokenfield

//= require backend/posts


$.ajaxSetup({
  headers: {
    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
  }
});
