$(document).ready(function(){
  var csrf_token = $('meta[name=csrf-token]').attr('content');
  var csrf_param = $('meta[name=csrf-param]').attr('content');
  var params;
  if (csrf_param !== undefined && csrf_token !== undefined) {
    params = csrf_param + "=" + encodeURIComponent(csrf_token);
  }

  $("#post_excerpt").redactor();
  $("#post_body").redactor({

    // autosave: '/backend/posts?' + params,
    // autosaveOnChange: true,

    // images
    imageUpload: '/uploads?' + params,
    imageManagerJson: '/uploads.json',
    imageUploadParam: 'upload[image]',

    // plugins
    plugins: ['imagemanager']
  });


  // tokenfield
  $('#post_tag_list').tokenfield();

  // adjust tokenfield
  $(".tokenfield").removeClass("form-control");
  $(".token-input").addClass("form-control");
  $(".token-input").removeAttr("style");
  $(".token-input").removeClass("token-input");


  // auto save posts
  // if ($(".post-editing-mode").length > 0) {
  //   $form = $(".post-editing-mode").find("form");
  //
  //   setInterval(function(){
  //     $form.submit();
  //     console.log("submit");
  //   }, 3000);
  // }
});
