// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
// require turbolinks



//= require jquery/jquery
//= require modernizr/modernizr
//= require bowser/bowser
//= require demography/demography
//= require sharpness/sharpness
//= require bootstrap-sass-official/assets/javascripts/bootstrap/transition
//= require bootstrap-sass-official/assets/javascripts/bootstrap/alert
//= require bootstrap-sass-official/assets/javascripts/bootstrap/button
//= require bootstrap-sass-official/assets/javascripts/bootstrap/carousel
//= require bootstrap-sass-official/assets/javascripts/bootstrap/collapse
//= require bootstrap-sass-official/assets/javascripts/bootstrap/dropdown
//= require bootstrap-sass-official/assets/javascripts/bootstrap/modal
//= require bootstrap-sass-official/assets/javascripts/bootstrap/tooltip
//= require bootstrap-sass-official/assets/javascripts/bootstrap/popover
//= require bootstrap-sass-official/assets/javascripts/bootstrap/scrollspy
//= require bootstrap-sass-official/assets/javascripts/bootstrap/tab
//= require bootstrap-sass-official/assets/javascripts/bootstrap/affix
//= require masonry/dist/masonry.pkgd

// require blueimp-gallery/js/blueimp-gallery
// require blueimp-gallery/js/blueimp-gallery-indicator
// require blueimp-gallery/js/jquery.blueimp-gallery
// require blueimp-bootstrap-image-gallery/js/bootstrap-image-gallery

//= require owl-carousel2/dist/owl.carousel

//= require main
