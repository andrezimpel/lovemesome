// hires images
$(document).ready(function(){
  $('[data-hires]').sharpness({
    browsers: ['msie8', 'msie9']
  });
});



// ----------------------------------------


// galleries
// $(document).ready(function(){
//   // add data gallery to all images within anchors
//   $(".article-content a img").each(function(){
//     $this = $(this);
//     $this.parent("a").attr("data-gallery", "");
//     $this.parent("a").attr("title", $this.attr("alt"));
//     $this.parent("a").addClass("article-gallery-anchor");
//   });
// });



// ----------------------------------------


// heros
$(document).ready(function() {
  var owl = $('.owl-carousel');
  owl.owlCarousel({
    stagePadding: 50,
    margin: 0,
    autoWidth: true,
    nav: true,
    center: true,
    loop: true,
    items: 2,
    autoplay: true,
    smartSpeed: 600,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
  })
})



// ----------------------------------------


// remove style attribute
$(document).ready(function(){
  $(".article-content p").removeAttr("style");
});



// ----------------------------------------


// article floats
$(window).load(function(){
  $('#articles-listing-excerpt').masonry({
    // columnWidth: 250,
    itemSelector: '.article-excerpt'
  });
});
