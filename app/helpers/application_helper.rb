module ApplicationHelper

  # markdown to html
  def markdown(content)
    @markdown ||= Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, space_after_headers: true, fenced_code_blocks: true)

    unless content == nil
      return @markdown.render(content).html_safe
    end
  end


  # website title
  def website_title(base, title)

    if title.present?
      return title + " | " + base
    end

    return base
  end
end
