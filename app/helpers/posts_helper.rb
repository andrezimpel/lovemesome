module PostsHelper

  def post_path(post)
    if post.published_at
      return "/#{post.published_at.strftime("%Y")}/#{post.published_at.strftime("%m")}/#{post.published_at.strftime("%d")}/#{post.handle}"
    else
      super
    end
  end

  def share_post_url(post)
    return "#{request.protocol}#{request.host}#{post_path(post)}"
  end
end
