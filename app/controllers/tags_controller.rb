class TagsController < ApplicationController
  def show
    tag_id = params[:id].to_i
    @tag = ActsAsTaggableOn::Tag.find(tag_id)

    @posts = Post.tagged_with(@tag.name)
    @title = @tag.name
  end
end
