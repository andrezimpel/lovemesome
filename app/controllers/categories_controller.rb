class CategoriesController < ApplicationController
  before_action :set_category, only: [:show]

  # GET /categories/1
  # GET /categories/1.json
  def show
    @posts = @category.posts
    @title = @category.title
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.where(handle: params[:id]).first
    end
end
