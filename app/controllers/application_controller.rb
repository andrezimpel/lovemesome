class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # set categories for the main navigation
  before_filter :set_categories
  def set_categories
    @categories = Category.all.active
  end

  # devise layout
  layout :layout_by_resource
  def layout_by_resource
    if devise_controller?
      'backend'
    else
      'application'
    end
  end
end
