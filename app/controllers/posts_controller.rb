class PostsController < ApplicationController
  before_action :set_post, only: [:show]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all.published
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @title = @post.title

    @post_next = @post.next
    @post_previous = @post.previous
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      if params[:id] != nil
        @post = Post.find(params[:id])
      else
        @post = Post.where(handle: params[:handle]).first
      end
    end
end
