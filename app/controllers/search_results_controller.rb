class SearchResultsController < ApplicationController
  def show
    search_query = params[:search]
    @search_query = search_query

    search = Post.search do
      fulltext search_query

      with(:published_at).less_than Time.now

      # order_by(:score, :desc)
      # order_by(:average_rating, :desc)
      # Sunspot.search Post, Tag do
    end

    @posts = search.results

    if @search_query == nil
      @title = "Search"
    else
      @title = "Searching for \"#{@search_query}\""
    end

  end
end
