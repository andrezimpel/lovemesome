require File.expand_path('../boot', __FILE__)

require 'rails/all'

require 'refile/rails.rb'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Lovemesome
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Berlin'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # assets
    config.assets.paths << Rails.root.join('vendor', 'assets', 'vendor/bower_components')

    # secret
    config.secret_token = "cd8e1b6005a82def82bc23351c5eed48983440ec3c94ce2ed2fe50ef84c8477709f58e7bc0587a6002c47785f124617214f48b00851d073435b3dcba52c3dfb5"

    # redis store
    config.cache_store = :redis_store, 'redis://localhost:6379/0/cache', { expires_in: 90.minutes }

    # refile redis cache
    config.after_initialize do
      Refile.app = Rack::Cache.new(Refile.app, metastore: 'redis://localhost:6379/0/metastore', entitystore: 'redis://localhost:6379/0/entitystore', verbose: true)
    end
  end
end
