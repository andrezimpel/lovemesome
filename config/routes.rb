Rails.application.routes.draw do

  # root
  get "/" => 'frontpage#show'

  resources :uploads



  # backend
  get "/backend" => redirect("/backend/posts"), as: "backend_root"
  namespace :backend do |backend|
    resources :posts
    resources :categories
  end



  # front end

  # posts
  resources :posts, only: [:index, :show]
  get "/:year/:month/:day/:handle" => 'posts#show'

  # categories
  resources :categories, only: [:show], path: "category"

  # tags
  get '/tags/:id' => 'tags#show', as: 'tag'

  # search
  get '/search' => 'search_results#show', as: "search_results"



  devise_for :users

  as :user do
    get "/login" => "devise/sessions#new", as: "user_login"
    get "/signin" => redirect("login")
    delete "/logout" => "devise/sessions#destroy"
  end

  root 'frontpage#show'
end
